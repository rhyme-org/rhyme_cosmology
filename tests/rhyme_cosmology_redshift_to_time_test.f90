logical function rhyme_cosmology_redshift_to_time_test () result (failed)
  use rhyme_cosmology

  implicit none

  type (cosmology_t) :: WMAP, EdS, MD_neg, MD_pos ! Matter dominated
  real(kind=8), parameter :: Om_neg = 0.3d0, Om_pos = 1.7d0
  real(kind=8) :: WMAP_age, MD_neg_age, MD_pos_age

  call WMAP%init(WMAP_cosmology)

  call EdS%init(EdS_cosmology)

  call MD_neg%init(EdS_cosmology)
  MD_neg%Om = Om_neg

  call MD_pos%init(EdS_cosmology)
  MD_pos%Om = Om_pos


  MD_neg_age = MD_neg%t_H%v * ( &
    1.d0 / (1.d0 - Om_neg) &
    - Om_neg / ( 2.d0 * (1.d0 - Om_neg)**1.5d0 ) * acosh( (2.d0 - Om_neg) / Om_neg ) &
  )

  MD_pos_age = MD_pos%t_H%v * ( &
    1.d0 / (1.d0 - Om_pos) &
    + Om_pos / ( 2.d0 * (Om_pos - 1.d0)**1.5d0 ) * acos( (2.d0 - Om_pos) / Om_pos ) &
  )

  WMAP_age = 137d2 ! +- 200

  failed = &
  abs(EdS%z_to_t(0.d0) - 2.d0 / 3.d0 * EdS%t_H%v) > epsilon(0.d0) &
  .or. abs(EdS%t_H%v - 1.d0 / EdS%H0%v * 3.086d19 / (1.d6 * 3.154d7)) > epsilon(0.d0) &
  .or. abs(Eds%z_to_t(1.d0) - 2.d0 / 3.d0 * EdS%t_H%v / 2.d0**1.5) > epsilon(0.d0) &
  .or. abs(MD_neg%z_to_t(0.d0) - MD_neg_age) > 1.d-10 &
  .or. abs(MD_pos%z_to_t(0.d0) - MD_pos_age) > 1.d-10 &
  .or. abs(WMAP%z_to_t(0.d0) - WMAP_age) > 2d2 ! WMAP precision
end function rhyme_cosmology_redshift_to_time_test
