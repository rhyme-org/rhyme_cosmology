logical function rhyme_cosmology_init_test () result (failed)
  use rhyme_cosmology

  implicit none

  type(cosmology_t) :: cosmo

  call cosmo%init(WMAP_cosmology)

  failed = &
  abs(cosmo%H0%v - 7.d1) > epsilon(0.d0) &
  .or. cosmo%H0%u%p() .ne. "km^1 s^-1 Mpc^-1" &
  .or. abs(cosmo%OL - 0.73d0) > epsilon(0.d0) &
  .or. abs(cosmo%Om - 0.27d0) > epsilon(0.d0) &
  .or. abs(cosmo%Ok - 0.d0) > epsilon(0.d0) &
  .or. abs(cosmo%Ob - 0.046d0) > epsilon(0.d0) &
  .or. abs(cosmo%Oc - 0.224d0) > epsilon(0.d0) &
  .or. abs(cosmo%t_H%v - 1.d0 / 7.d1 * 3.086d19 / (1.d6 * 3.154d7)) > epsilon(0.d0)
end function rhyme_cosmology_init_test
