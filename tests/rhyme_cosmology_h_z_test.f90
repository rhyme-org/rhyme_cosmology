logical function rhyme_cosmology_h_z_test () result (failed)
  use rhyme_cosmology

  implicit none

  type(cosmology_t) :: WMAP
  real(kind=8) :: H0, H1

  call WMAP%init(WMAP_cosmology)

  H0 = WMAP%H0%v * sqrt(WMAP%Om + WMAP%OL)
  H1 = WMAP%H0%v * sqrt(WMAP%Om * 2.d0**3 + WMAP%OL)

  failed = &
  abs(WMAP%H(0.d0) - H0) > epsilon(0.d0) &
  .or. abs(WMAP%H(1.d0) - H1) > epsilon(0.d0)
end function rhyme_cosmology_h_z_test
