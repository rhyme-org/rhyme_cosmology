module rhyme_cosmology
  use rhyme_nombre

  implicit none

  integer, parameter :: EdS_cosmology = 1
  integer, parameter :: WMAP_cosmology = 2
  integer, parameter :: PLANCK15_cosmology = 3

  type cosmology_t
    type(nombre_t) :: H0, t_H, rho0
    real(kind=8) :: OL, Om, Ob, Oc, Ok
  contains
    procedure :: init => rhyme_comsology_init
    procedure :: z_to_t => rhyme_cosmology_redshift_to_time
    procedure :: t_to_z => rhyme_comsology_time_to_redshift
    procedure :: H => rhyme_cosmology_H_z
  end type cosmology_t

contains

  subroutine rhyme_comsology_init (this, cosmology)
    implicit none

    class(cosmology_t), intent(inout) :: this
    integer, intent(in) :: cosmology

    if ( cosmology .eq. EdS_cosmology ) then
      this%H0 = 7.d1 .u. (kilo * m) / s / (Mega * pc)
      this%t_H = 1.d0 / this%H0
      this%OL = 0.d0
      this%Om = 1.d0
      this%Ob = 1.d0
      this%Oc = 0.d0
    else if ( cosmology .eq. WMAP_cosmology ) then
      this%H0 = 7.d1 .u. (kilo * m) / s / (Mega * pc)
      this%t_H = 1.d0 / this%H0
      this%OL = 0.73d0
      this%Om = 0.27d0
      this%Ob = 0.046d0
      this%Oc = 0.224d0
    end if

    this%t_H = this%t_H .to. (Mega * yr)
    this%Ok = 1.d0 - this%Om - this%OL
  end subroutine rhyme_comsology_init


  real(kind=8) function rhyme_cosmology_redshift_to_time (this, z) result (t)
    implicit none

    class(cosmology_t), intent(in) :: this
    real(kind=8), intent(in) :: z

    if ( this%OL > 0.d0 ) then ! Lambda-CDM
      t = this%t_H%v * 2.d0 / 3.d0 * log( &
        (sqrt(this%OL * ( this%Om + this%OL / (1.d0 + z)**3.d0 ) ) + this%OL / (1.d0 + z)**1.5d0) &
        / sqrt(this%OL * this%Om) &
      ) / sqrt(this%OL)
    else if ( abs(this%OL - 0.d0) <= epsilon(0.d0) ) then
      if ( abs(this%Om - 1.d0) <= epsilon(0.d0) ) then ! EdS Universe
        t = this%t_H%v * 2.d0 / 3.d0 / (1.d0 + z)**1.5d0
      else if ( this%Om < 1 ) then ! Matter-dominated Univers with Om < 1
        t = this%t_H%v * this%Om / ( 2.d0 * (1.d0 - this%Om)**1.5d0 ) * ( &
        2.d0 * sqrt( (1.d0 - this%Om) * (z * this%Om + 1.d0) ) / (z * this%Om + this%Om) &
        - acosh( (z * this%Om - this%Om + 2.d0) / (z * this%Om + this%Om) ) &
        )
      else if ( this%Om > 1 ) then ! Matter-dominated Univers with Om > 1
        t = this%t_H%v * this%Om / ( (-2.d0) * (this%Om - 1.d0)**1.5d0 ) * ( &
        2.d0 * sqrt( (this%Om - 1.d0) * (z * this%Om + 1.d0) ) / (z * this%Om + this%Om) &
        - acos( (z * this%Om - this%Om + 2.d0) / (z * this%Om + this%Om) ) &
        )
      end if
    end if
  end function rhyme_cosmology_redshift_to_time


  real(kind=8) function rhyme_comsology_time_to_redshift (this, t) result (z)
    implicit none

    class(cosmology_t), intent(in) :: this
    real(kind=8), intent(in) :: t

    real(kind=8) :: B, C
    integer :: ifail = 1

    B = 0.d0
    C = 1.d6

    call fzero (fzero_func, B, C, 1.d-7, 1.d-7, ifail)

    z = (B + C) / 2
  contains

    real(kind=8) function fzero_func (z) result (dt)
      implicit none

      real(kind=8), intent(in) :: z

      dt = rhyme_cosmology_redshift_to_time(this, z) - t
    end function fzero_func
  end function rhyme_comsology_time_to_redshift


  real(kind=8) function rhyme_cosmology_H_z (this, z) result (H_z)
    implicit none

    class(cosmology_t), intent(in) :: this
    real(kind=8), intent(in) :: z

    real(kind=8) :: z_p1

    z_p1 = (1.d0 + z)

    H_z = this%H0%v * sqrt( this%Om * z_p1**3 + this%Ok * z_p1**2 + this%OL )
  end function rhyme_cosmology_H_z

end module rhyme_cosmology
