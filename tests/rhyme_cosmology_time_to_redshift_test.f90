logical function rhyme_cosmology_time_to_redshift_test () result (failed)
  use rhyme_cosmology

  implicit none

  type(cosmology_t) :: WMAP

  call WMAP%init(WMAP_cosmology)

  failed = &
  abs(WMAP%t_to_z(6020.d0) - 1.d0) > 1.d-2 &
  .or. abs(WMAP%t_to_z(13875.d0) - 0.d0) > 1.d-2
end function rhyme_cosmology_time_to_redshift_test
